# Crear una nueva lista vacía
$miLista = New-Object System.Collections.Generic.List[string]

# Agregar elementos a la lista

$miLista.Add("MySQL")
$miLista.Add("PostgreSQL")
$miLista.Add("Microsoft SQL Server")
$miLista.Add("Oracle Database")
$miLista.Add("SQLite")
$miLista.Add("MongoDB")
$miLista.Add("MariaDB")
$miLista.Add("IBM Db2")
$miLista.Add("Amazon Aurora")
$miLista.Add("Firebase Realtime Database")
$miLista.Add("Couchbase")
$miLista.Add("Redis")
$miLista.Add("Cassandra")
$miLista.Add("Microsoft Access")
$miLista.Add("SQLite")

# Iterar a través de los elementos de la lista
foreach ($elemento in $miLista) {
    if (Test-Path -Path "containers" -PathType Container) {
        Set-Location .\containers
        New-Item -Type Directory $elemento
        Set-Location $elemento
        New-Item -Type File docker-compose.yml
        Write-Output "version: " > docker-compose.yml
        Write-Output "services:" >> docker-compose.yml
        
        New-Item -Type Directory Database
        Set-Location ../
        Set-Location ../
    }
    else {
        New-Item -Type Directory "containers"
    }
}

