# Containers

- ir al directorio de la base de datos
- llegar hasta donde esxiste un archivo docker-compose.yml

- Comando para ejecutar un servicio en docker

```
docker-compose up -d

# o

docker-compose up -d <nombre-servicio>
```

- Programa parta conectarse a la base de datos [DBeaver](https://dbeaver.io/)

Aqui esta una tabla con las bases de datos y los puertos en los que generalmente corren:

| Base de Datos        | Puerto por Defecto |
| -------------------- | ------------------ |
| MySQL                | 3306               |
| PostgreSQL           | 5432               |
| Microsoft SQL Server | 1433               |
| Oracle Database      | 1521               |
| MongoDB              | 27017              |
| SQLite               | No utiliza puertos |
| MariaDB              | 3306               |
| Redis                | 6379               |
| CouchDB              | 5984               |
| Cassandra            | 9042               |
| Neo4j                | 7687               |
| Firebase Realtime DB | No utiliza puertos |
